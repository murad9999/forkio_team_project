# FORKIO_TEAM_PROJECT

```
cd existing_repo
git remote add origin https://gitlab.com/murad9999/forkio_team_project.git
git branch -M main
git push -uf origin main
```

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

---

## Name

Forkio Project.

## Description

This project is a team project which the members of it are Murad Masimli and Sahib Humbatzada. This project is given by the course organized by DAN IT. Thanks to this company, for providing this type of figma designs and helping us to enhance designing skills with HTML and SCSS. More details about the design of the project is noted below!

## Installation

This repository is installed by copying the url and cloning with the help of git. Having cloned into local computer, it is necessary to install node_modules by writing `npm install`. After that, by running `npm run dev`, you can start the project in the localhost.



## Authors

1. Sahib Humbatzada
2. Murad Masimli

## GENERAL TASK

In order to compile and minifying js, sass, css and images, gulp is utilized. There are lots of tecnhologies related to gulp which are used in this project. These libraries are located in gulpfile.js. All codes of compiling is there. These are libraries belonging to gulp.

1. gulp
2. gulp-sass
3. gulp-uglify
4. gulp-clean-css
5. gulp-clean
6. gulp-concat
7. gulp-imagemin
8. gulp-autoprefixer

In addition, browser-sync was configured for reloading in every changes.

## Student 1 (Sahib Humbatzada)

1.I did task for dropdown menu, so that after designing html code, I put JS code to work burger-menu then add all SCSS designs for this. (feature/dropdown belongs to me!)

2.Secondly, I just did the section of "People are talking about fork!" with structuring elements on HTML and did their design by using SCSS. (feature/talking belongs to me!)

## Student 2 (Murad Masimli)

1. The task given for me is to code main and footer section with the help of scss (css preprocessor). The main part also is divided two sections: 'revolutionary editor' and 'here is what you get'

2. The second one is the footer. The footer actually does not like that in other websites. There are four cards which are about subsciption pricing. It is not hard to create it because of having scss advantages.

The branch `feature/revolutionary-ediot` belongs to me.



## Contributing

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.