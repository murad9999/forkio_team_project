/* GENERAL */
var gulp = require('gulp')
var concat = require('gulp-concat')
var clean = require('gulp-clean')
var browserSync = require('browser-sync').create();


/* FOR MINIFYING JS */
var uglify = require('gulp-uglify');



/* FOR MINIFYING SCSS */
var sass = require('gulp-sass')(require('sass'));
var cleanCSS = require('gulp-clean-css');
var autoprefixer = require('gulp-autoprefixer');



/* FOR OPTIMIZING IMAGES */
const imagemin = require('gulp-imagemin');








/* PARTS OF BUILD */

// 1. CLEAR DIST FOLDER
gulp.task('clear', () => {
    return gulp.src('dist/*', { read: false }).pipe(clean())
})

// 2. HANDLING CSS
gulp.task('minifyCss', () => {
    return gulp.src('src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError)) //COMPILE SCSS TO CSS
        .pipe(autoprefixer({

            cascade: false
        })) //AUTOPREFIXING FOR THE LAST TWO VERSIONS OF EACH BROWSERS 
        .pipe(cleanCSS({ compatibility: 'ie8' })) //REMOVING UNUSED CSS
        .pipe(concat('styles.min.css')) //Concatenating
        .pipe(gulp.dest('dist'))
})

// 3. MINIFYING JS
gulp.task('minifyJs', () => {
    return gulp.src('src/js/*.js')
        .pipe(uglify()) //AUTOPREFIXING

        .pipe(concat('scripts.min.js')) //Concatenating
        .pipe(gulp.dest('dist'))

})

// 4. OPTIMIZING IMAGES
gulp.task('optimizeImages', () => {
    return gulp.src("src/img/**/*")
        .pipe(imagemin()) //OPTIMIZING
        .pipe(gulp.dest('dist/img'))
})




/* BUILD TASK */
gulp.task('build', gulp.series(['clear', 'minifyCss', 'minifyJs', 'optimizeImages']))



/* DEVELOPMENT TASK */
gulp.task('startServer', function () {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        index: 'index.html'
    })
    gulp.watch('src/scss/**/*.scss', gulp.series(['minifyCss'])).on('change', browserSync.reload);
    gulp.watch('src/js/**/*.js', gulp.series(['minifyJs'])).on('change', browserSync.reload);
    gulp.watch('src/img/*', gulp.series(['optimizeImages'])).on('change', browserSync.reload);
});

/* DEVELOPMENT TASK */
gulp.task('dev', gulp.series(['build', 'startServer']))
